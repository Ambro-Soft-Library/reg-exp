exports.generateValues = (...testValues) => {
  return testValues.map(v => {
    const message = (v === '') ? 'EMPTY string (\"\")' : v;
    return {value: v, msg: `Test error on value: ${message}`}
  });
}

exports.generateTestName = (name, correctCases) => {
  const descrip = correctCases ? 'correct' : 'incorrect'
  return `${name} - ${descrip} values`
}