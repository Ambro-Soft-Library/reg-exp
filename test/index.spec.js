import test from 'ava'
import regexp from '../src'
import { generateValues, generateTestName } from './helper'

test(generateTestName('integer', true), t => {
  const values = generateValues('-0', '-00', '-010', '-1', '-9', '-102', '0',  '00',  '010',  '1',  '9',  '102')
  values.forEach(elem => {
    t.true(regexp.integer.test(elem.value), elem.msg)
  })
})

test(generateTestName('integer', false), t => {
  const values = generateValues('-', '--0', '--1', '--9', '--102', '0-', '1-', '9-', '102-', '0a', 'a', '1a0')
  values.forEach(elem => {
    t.false(regexp.integer.test(elem.value), elem.msg)
  })
})

test(generateTestName('double', true), t => {
  const values = generateValues('-0', '-00', '-001', '-0.0', '-0.25', '-1', '-10.25', '0', '00', '001', '0.0', '0.25', '1', '10.25')
  values.forEach(elem => {
    t.true(regexp.double.test(elem.value), elem.msg)
  })
})

test(generateTestName('double', false), t => {
  const values = generateValues('', '--0', '--1', '--9', '--102', '0-', '1-', '9-', '102-', '0a', 'a', '1a0',
                                '-', '-0.', '0.', '00.', '001.', '0.0.', '.0', '.25', '10.')
  values.forEach(elem => {
    t.false(regexp.double.test(elem.value), elem.msg)
  })
})

test(generateTestName('email', true), t => {
  const values = generateValues('d@d.co', '?@d.co', 'rame@mail.com')
  values.forEach(elem => {
    t.true(regexp.email.test(elem.value), elem.msg)
  })
})

test(generateTestName('email', false), t => {
  const values = generateValues('', '@', '.', 'a@', '@a', 'a@a.', 'a@a.c', ':a@a.co', '.a@a.co', 'email', 'e.com', 'a@a@d.co', 'd@d.co@', '@d@d.co')
  values.forEach(elem => {
    t.false(regexp.double.test(elem.value), elem.msg)
  })
})

test(generateTestName('percent', true), t => {
  const values = generateValues('100', '100.0', '100.00', '0', '0.1', '0.00000000', '0.12345678', 
                                '1', '1.1', '10', '10.1', '10.12345678')
  values.forEach(elem => {
    t.true(regexp.percent.test(elem.value), elem.msg)
  })
})

test(generateTestName('percent', false), t => {
  const values = generateValues('1000', '1000.', '101', '101.', '100.', '100.000', '00.', '0.', '00', '00.', 
                                '00.1', '01', '01.', '015', '015.', '0.1.', '1.', '10.',
                                '', '-', '-0', '-0.1', '-0.01', '-1', '-1.25', '-10', '-10.25', 
                                'a', '0a', '.')
  values.forEach(elem => {
    t.false(regexp.percent.test(elem.value), elem.msg)
  })
})

test(generateTestName('amount', true), t => {
  const values = generateValues('0', '00', '00.00', '00.25', '0.0', '0.00', '0.25', 
                                '-0', '-00', '-0.00', '-00.00', '-0.01', '-00.01', '-1', '-1.01', '-1.0', '-100',
                                '1', '1.0', '1.00', '1.25', '100.0', '100.00', '100.25', '1.100', '100.001', '10.900', '-10.900')
  values.forEach(elem => {
    t.true(regexp.amount.test(elem.value), elem.msg)
  })
})

test(generateTestName('amount', false), t => {
  const values = generateValues('', '-', '0.', '00.', '01', '0.1.', '.25', '1.', '100.')
  values.forEach(elem => {
    t.false(regexp.amount.test(elem.value), elem.msg)
  })
})

test(generateTestName('password', true), t => {
  const values = generateValues('Passw0rd', '1234Task', 'Geo123gia')
  values.forEach(elem => {
    t.true(regexp.password.test(elem.value), elem.msg)
  })
})

test(generateTestName('password', false), t => {
  const values = generateValues('pass', '12345678', 'geo123gia', 'BIG_PASSWORD')
  values.forEach(elem => {
    t.false(regexp.password.test(elem.value), elem.msg)
  })
})

test(generateTestName('descrip.de', true), t => {
  const values = generateValues('test testadze', 'äöüßÄÖÜẞ')
  values.forEach(elem => {
    t.true(regexp.descrip.de.test(elem.value), elem.msg)
  })
})

test(generateTestName('descrip.de', false), t => {
  const values = generateValues('', '  ', '\n\n', '\t\t', '\ntest', '\ttest', ' test', 'test\n', 'test\t', 'test ')
  values.forEach(elem => {
    t.false(regexp.descrip.de.test(elem.value), elem.msg)
  })
})

test(generateTestName('word.en', true), t => {
  const values = generateValues('word', 't90est', 'w_ord')
  values.forEach(elem => {
    t.true(regexp.word.en.test(elem.value), elem.msg)
  })
})

test(generateTestName('word.en', false), t => {
  const values = generateValues('3word', '_test', '998908', ';?!', 'w;ord')
  values.forEach(elem => {
    t.false(regexp.word.en.test(elem.value), elem.msg)
  })
})

test(generateTestName('word.en_hyphen', true), t => {
  const values = generateValues('abc', 'a123', 'a1b2', 'abcdabcdabcdabcd',  'ab-cd',  'ab_cd',  
                                'ab-12-cd',  'ab-12_cd',  'ab_12-cd',  'ab_12_cd',
                                'ab--cd', 'ab__cd', 'ab-_cd')
  values.forEach(elem => {
    t.true(regexp.word.en_hyphen.test(elem.value), elem.msg)
  })
})

test(generateTestName('word.en_hyphen', false), t => {
  const values = generateValues('', '1abc', 'ab cd', 'ab.cd',  
                                'ab?cd', 'ab=cd', 'ab+cd', 'ab,cd', 'ab!cd', 'ab;cd', 'ab@cd', 'ab#cd',
                                'ab$cd', 'ab%cd', 'ab^cd', 'ab&cd', 'ab*cd', 'ab(cd', 'ab)cd', 'ab+cd',
                                'ab=cd', 'ab|cd', 'ab/cd', 'ab\\cd', 'cd<cd', 'ab>cd')
  values.forEach(elem => {
    t.false(regexp.word.en_hyphen.test(elem.value), elem.msg)
  })
})