/**
 * Expresion for integer numbers, range is (-infinity, infinity)
 */
export const integer: RegExp

/**
 * Expression for double numbers, range is (-infinity, infinity)
 */
export const double: RegExp

/**
 * Expression for email
 */
export const email: RegExp

/**
 * Expression for percent
 * @example 100; 0.05; 0.55
 */
export const percent: RegExp

/**
 * Expression for amount (negaitve or positive value)
 */
export const amount: RegExp

/**
 * Expression for password strength (At least one letter: uppercase & lowecase & number).
 * The min length is 1 and the max length is not limited.
 */
export const password: RegExp

/**
 * @type {object} descrip
 * @property {RegExp} descrip.de - Expression for german text
 */
export const descrip: object = {
  de
}

/**
 * @type {object} word
 * @property {RegExp} word.en - Expression for latin word. The first letter will be from English alphabet.
 * @property {RegExp} word.en_hyphen - Expression for latin word with hyphens (-). The first letter will be from English alphabet.
 */
export const word: object = {
  en, en_hyphen
}

